using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class GamePreferencesData
{
    public float _mouseSentivity;
    public bool _isMaster;
    public bool _isMusic;
    public bool _isSFX;
    public float _masterVolume;
    public float _musicVolume;
    public float _SFXVolume;
}
