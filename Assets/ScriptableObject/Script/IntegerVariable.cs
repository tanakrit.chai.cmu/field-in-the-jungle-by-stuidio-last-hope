using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastHopeStudio
{
    [CreateAssetMenu]
    public class IntegerVariable : ScriptableObject
    {
        public int Value;
    }
}