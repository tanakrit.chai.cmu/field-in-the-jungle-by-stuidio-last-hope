using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastHopeStudio
{
    [CreateAssetMenu]
    public class FloatVariable : ScriptableObject
    {
        public float Value;
    }
}